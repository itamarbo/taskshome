<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Task;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Response;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         //$tasks = Task::all();

        // $id=1;
        // $tasks = User::find($id)->tasks; //show the tasks only of user number 'id'
       //  return view('tasks.index', ['tasks' => $tasks]);
       
       $id = Auth::id();
       if (Gate::denies('manager')) {
           $boss = DB::table('employees')->where('employees',$id)->first();
           $id = $boss->manager;
       }
       $user = User::find($id);
       $tasks = $user->tasks;
       return view('tasks.index', compact('tasks'));
 
      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create tasks..");
        }
        
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create tasks..");
        }
        $task = new Task();
        $id = Auth::id();
        $task->title = $request->title;
        $task->user_id = $id;
        $task->status =0;
        $task->save();
        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create tasks..");
        }
        $task = Task::find($id);
        return view("tasks.edit", ['task' => $task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*
        $task = Task::find($id);
        if(!$task->user->id == Auth::id()) return(redirect('tasks'));
        $task->update($request->except(['_token'])); 
        if($request->ajax()){
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }
        return redirect('tasks');
         */
        
        {
            //only if this task belongs to user
            $task = Task::findOrFail($id);
           //regular users are not allowed to change the title 
            if (Gate::denies('manager')) {
                if ($request->has('title'))
                       abort(403,"You are not allowed to edit tasks..");
            }   
        
            //make sure the task belongs to the logged in user
            if(!$task->user->id == Auth::id()) return(redirect('tasks'));
       
            //test if title is dirty
           
            $task->update($request->except(['_token']));
       
            if($request->ajax()){
                return Response::json(array('result' => 'success1','status' => $request->status ), 200);   
            } else {          
                return redirect('tasks');           
            }
        }
       

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        }
        $task = Task::find($id);
        if(!$task->user->id == Auth::id()) return(redirect('tasks'));
        $task->delete();
        return redirect('tasks');

    }

    public function mytasks()
    { 
        $id = Auth::id();
        $user = User::Find($id);
        $tasks = $user->tasks;
        return view('tasks.index', ['tasks' => $tasks]);
        
    }
}
