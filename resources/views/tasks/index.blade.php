@extends('layouts.app')
@section('content')


<h1>This is a tasks list</h1>
@if (Request::is('tasks'))  

<h2><a href="{{action('TaskController@mytasks')}}">My Tasks</a></h2>
@else
<h2><a href="{{action('TaskController@index')}}">Show All Tasks</a></h2>

@endif


<ul>
{{-- הערה --}}

    @foreach($tasks as $task)
    <li>
    
    @can('manager')
    @if ($task->status)
           <button id ="" value="1"> Done!</button>
       @else
           <button style="text-decoration: underline" id ="{{$task->id}}" value="0"> Mark as done</button>
       @endif
       @endcan
       
        id: {{$task->id}} {{$task->title}}
        
        @can('manager')
        <a href = "{{route('tasks.edit', $task->id)}}"> Edit this task </a>
        
     {{-- כפתור למחיקה --}}
     <form method = 'post' action = "{{action('TaskController@destroy', $task->id)}}" >
        @csrf      
        @method('DELETE')    
        <div class = "form-group">    
            <input type = "submit" class = "btn btn-primary" name = "submit" value = "Delete">
        </div>

        </form>
        @endcan
            </li> <br>
            @endforeach
        </ul>

<a href ="{{route('tasks.create')}}">Create a new task</a>

<script>
       $(document).ready(function(){
           $("button").click(function(event){
               $.ajax({
                   url:  "{{url('tasks')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
               location.reload();

           });

       });
   </script>
@endsection
