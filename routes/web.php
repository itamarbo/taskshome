<?php

use App\Task;
use App\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
Route::get('/read', function(){

    $tasks = Task::all(); // שולף את כל הרשומות מטבלת פוסטס
    foreach($tasks as $task){

        echo $task->title ."<br>";
        
    }


});


// one to many relationship
Route::get('/alltasks', function(){

    $user = User::find(1);

    foreach($user->tasks as $task){
      echo  $task->title ."<br>";
    
    }
});


//show only who has status=1 at tasks table
Route::get('/read', function(){

    $tasks = Task::all(); // שולף את כל הרשומות מטבלת פוסטס
    $tasks = DB::select('select * from tasks where status = ?', [1]);
    foreach($tasks as $task){

        echo $task->title ."<br>";
        
    }


});
*/

Route::resource('tasks', 'TaskController')->middleware('auth');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/mytasks', 'TaskController@mytasks');